# Testing in Haskell

## Getting ready for later

``` shell
git clone git@github.com:NorfairKing/haskell-testing-intro.git
cd haskell-testing-intro
git checkout 5e218d0
stack test --file-watch
```

## Why listen to me?

* Testing is what I do.
* My services have more uptime than their uptime monitor.

## Why testing?

Why do we program? We make something happen!

So what's the problem? That turns out to be hard to do.

We would like to be confident that the right things will happen.

## Making things that work

1. Choose the right language:
   Parsing, Types, Compiler, ...

1. Formal methods? Maybe.

1. Testing                <-- You are here

## What is testing

Give code as much opportunity as possible to break during development, so we can fix things before the code breaks for customers.

* Testing gives your code *an opportunity* to break earlier.
* Testing lets you *provide evidence* that your code is not broken in the ways that you check for.
* Testing helps you to check that your code *doesn't not* work. (Note the intentional double negative.)
* Testing lets you *falsify* that you should deploy.

## What is a test (attempt 1)

```
calculateA :: Char
calculateA = 'b'

test :: Bool
test = 'a' == calculateA
```

. . .

```
test failed: False
```

## What is a test (attempt 2)

```
calculateA :: Char
calculateA = 'b'

test :: IO ()
test = calculateA `shouldBe` 'a'
```

. . .

```
  ✗ 1 example
      Expected these values to be equal:
      Actual:   'b'
      Expected: 'a'
```

. . .

*Conclusion: A test is a piece of code that is considered "passing" if it does not crash.*

## Writing a test

``` haskell
spec :: Spec -- A 'Spec' is a test suite, or "specification"
spec =
  describe "calculateA" $
    it "does what I want with 'a'" $
      calculateA `shouldBe` 'a' :: IO ()

-- Just to show that it is indeed 'IO ()'
shouldBe :: (Show a, Eq a) => a -> a -> IO ()
```

## Basic workflow

1. Set up a feedback loop. (Am I done?)
1. Write a test that makes the feedback loop fail.
1. Write the code that makes that feedback loop pass.

Same workflow for development, bug fixing, performance optimisation

## Try it out (unit test)

* Reversing `[1, 2]` turns it into `[2, 1]`.
* The absolute value of `-1` is `1`.

## Property testing (intro)

Sometimes a value "should" not matter:

``` haskell
spec :: Spec
spec =
  describe "reverse" $
    it "produces the original list after reversing twice"
      reverse (reverse [1, 2]) `shouldBe` [1, 2]
```

## Property testing for dummies

Dummy values, that is:

``` haskell
spec :: Spec
spec =
  describe "reverse" $ do
    it "produces the original list after reversing twice" $ do
      let ls = [1, 2] -- "should" not matter
      reverse (reverse ls) `shouldBe` ls

    -- Now we also test that it does not matter:
    it "produces the original list after reversing twice" $ 
      -- Read this as "For every ls ..."
      property $ \ls ->
        -- "... this test should pass."
        reverse (reverse ls) `shouldBe` ls
```

## Property testing in general

``` haskell
-- Just a test
myTest :: IO ()

-- Property test
myPropertyTest :: Argument -> IO ()
```

We use *randomness* to supply the `Argument`.

## Spot the property (1)

``` haskell
spec :: Spec
spec =
  describe "abs" $
    it "is positive" $ do
      abs 5 `shouldSatisfy` (> 0)
```

. . .

``` haskell
spec :: Spec
spec =
  describe "abs" $
    it "is positive" $ do
      let i = 5
      abs i `shouldSatisfy` (> 0)
```

. . .

``` haskell
spec :: Spec
spec =
  describe "abs" $
    it "is positive" $ do
      property $ \i ->
        abs i `shouldSatisfy` (> 0) -- Fails for `0`!
```

## Spot the property (2)

``` haskell
spec :: Spec
spec =
  describe "abs" $
    it "is unphased by negation" $ do
      abs 6 `shouldBe` abs (-6)
```

. . .

``` haskell
spec :: Spec
spec =
  describe "abs" $
    it "is unphased by negation" $ do
      abs 6 `shouldBe` abs (negate 6)
```

. . .

``` haskell
spec :: Spec
spec =
  describe "abs" $
    it "is unphased by negation" $ do
      let i = 6
      abs i `shouldBe` abs (negate i)
```

. . .

``` haskell
spec :: Spec
spec =
  describe "abs" $
    it "is unphased by negation" $ 
      property $ \i -> 
        abs i `shouldBe` abs (negate i)
```

## Try it out (property test)

* Reversing a list twice does nothing.
* Absolute value is idempotent. (Twice is the same as once.)

## What to test?

1. External expectations of your code:

``` haskell
spec :: Spec
spec = 
  describe "doTheThing" $ 
    it "does the thing" $ 
      doTheThing
      didTheThingHappen <- getTheResult
      didTheThingHappen `shouldBe` True
```

. . .

2. Regressions

## What not to test?

Spot the mistake:

``` haskell
spec :: Spec
spec = 
  describe "abs" $
    it "makes the number an absolute value" $
      property $ \i ->
        let expectedResult = if i >= 0 then i else negate i
        abs i `shouldBe` expectedResult
```

. . .

*Don't reimplement the code in the tests*

## When to stop testing?

1. Imagine code that passes your tests but is still wrong.
1. Estimate how difficult that code is to write.
1. More difficult than the correct code? -> you're done.


## Conclusion

1. Testing is important
1. Testing is not difficult
1. Start with tests or you will embed bias in your tests.
1. Start with tests or they will never happen.

## Further reading:

Mocking: https://cs-syd.eu/posts/2021-10-22-why-mocking-is-a-bad-idea
Test Pollution: https://cs-syd.eu/posts/2021-10-23-test-pollution
Portfolio: https://cs-syd.eu/posts/2021-10-24-your-testing-portfolio
Property testing overview: https://www.fpcomplete.com/blog/quickcheck-hedgehog-validity/
